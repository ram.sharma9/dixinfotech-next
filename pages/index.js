import Head from 'next/head'
import Header from './components/Header'
import Banner from './components/Banner'
import AboutSection from './components/AboutSection'
import AboutBar from './components/AboutBar'
import ServiceSection from './components/ServiceSection'
import TechnologySection from './components/TechnologySection'
import DomainSection from './components/DomainSection'
import ProjectSection from './components/ProjectSection'
import TestimonialSection from './components/TestimonialSection'
import FooterForm from './components/FooterForm'
import Footer from './components/Footer'
import { useEffect, useState } from "react";

export default function Home() {

  const [pageOffset, setpageoffset] = useState(0);
  useEffect(() => {
    window.onscroll = () => {
      setpageoffset(window.pageYOffset)
    }
  }, []);

  return (
    <>
      <Head>
        <title>Dixinfotech LLP</title>
        <meta name="description" content="Wesite Development Company" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <Header offset={pageOffset} />
      <Banner />
      <AboutSection />
      <AboutBar offset={pageOffset} />
      <ServiceSection />
      <TechnologySection />
      <DomainSection />
      <ProjectSection />
      <TestimonialSection />
      <FooterForm />
      <Footer />
    </>
  )
}
