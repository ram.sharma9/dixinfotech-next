import React from "react";
import Link from "next/link";
import Image from "next/image";
import logo from "../../public/logo.png";
import styles from "../../styles/Header.module.css";
import { Button } from "@material-ui/core";
import PhoneIcon from "@material-ui/icons/Phone";
import MenuIcon from "@material-ui/icons/Menu";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
});

function Header(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({ right: false });
  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setState({ ...state, ["right"]: open });
  };

  return (
    <header
      className={`${styles.header} header container-fluid`}
      style={{
        backgroundColor: `${props.offset > 50 ? "rgba(0,0,0,.89)" : ""}`,
      }}
    >
      <div className="container">
        <div className="row py-4 py-md-3">
          <div className="col-6 col-md-3 px-0 px-md-3 d-flex justify-content-center align-items-center">
            <Link href="/">
              <a>
                <Image
                  src={logo}
                  className={styles.logo}
                  alt=""
                  width={200}
                  height={60}
                />
              </a>
            </Link>
          </div>
          <div className="col-6 col-md-9 d-flex justify-content-end align-items-center">
            <Link href="tel:+919717106162">
              <Button
                variant="outlined"
                color="primary"
                size="large"
                startIcon={<PhoneIcon />}
                className={`${styles.call_btn} d-none d-md-inline-flex`}
              >
                : +91-9717106162
              </Button>
            </Link>
            <Link href="tel:+919717106162">
              <Button
                variant="outlined"
                color="secondary"
                size="large"
                className={`${styles.quote_btn} ml-3 d-none d-md-inline-flex`}
              >
                Get Free Qoute
              </Button>
            </Link>

            <React.Fragment key="right">
              <MenuIcon
                className={`${styles.menu_btn} ml-4 cursor-pointer`}
                onClick={toggleDrawer(true)}
              />
              <Drawer
                anchor="right"
                open={state["right"]}
                onClose={toggleDrawer(false)}
              >
                <div
                  className={clsx(classes.list, {
                    [classes.fullList]:
                      "right" === "top" || "right" === "bottom",
                  })}
                  role="presentation"
                  onClick={toggleDrawer(false)}
                  onKeyDown={toggleDrawer(false)}
                >
                  <List>
                    <ListItem>
                      <ListItemText primary="Our Services" />
                    </ListItem>
                    <Divider />
                    <ListItem button>
                      <ListItemText secondary="Corporate Identity Design" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Ecommerce Website Design" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Landing Page Design" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Logo Design" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Mobile Apps UI Design" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Mobile Site Design" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Responsive Website Design" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Ecommerce Website Development" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Magento Development" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="WordPress Development" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Shopify Development" />
                    </ListItem>

                    <ListItem button>
                      <ListItemText secondary="Angular JS" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Laravel Development" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Joomla Development" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Ecommerce Marketing" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Google Local Business SEO Services" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Youtube Video Marketing" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="SMS Marketing" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Email Marketing" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Search Engine Marketing – PPC" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText secondary="Social Media Optimisation" />
                    </ListItem>
                  </List>
                </div>
              </Drawer>
            </React.Fragment>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
