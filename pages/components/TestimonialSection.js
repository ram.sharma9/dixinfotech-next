import Image from 'next/image'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import testimonial1 from "../../public/testimonial1.jpg"
import testimonial2 from "../../public/testimonial2.png"
import styles from '../../styles/TestimonialSection.module.css'

function TestimonialSection() {
    const settings = {
        dots: false,
        infinite: true,
        autoplay: true,
        speed: 500,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    };

    return (
        <section className="container py-lg-5">
            <div className="row">
                <div className="col-12">
                    <Slider {...settings}>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                    <Image src={testimonial2} fill="responsive" />
                                </div>
                                <div className="col-lg-6">
                                    <h6 className={`${styles.subHeading} mb-0`}>OUR TESTOMONIAL</h6>
                                    <h2 className={`${styles.heading} mt-2 mb-0`}>Our clients simply love our work</h2>
                                    <p className={`${styles.para}`}>Ram and his team were always there when we needed them. The ideas and suggestions provided were such that we could get the best quality work done with least damage to our budget </p>
                                    <span className={`${styles.name}`}>Varun Harnal</span>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                    <Image src={testimonial2} fill="responsive" />
                                </div>
                                <div className="col-lg-6">
                                    <h6 className={`${styles.subHeading} mb-0`}>OUR TESTOMONIAL</h6>
                                    <h2 className={`${styles.heading} mt-2 mb-0`}>Our clients simply love our work</h2>
                                    <p className={`${styles.para}`}>Ram and his team were always there when we needed them. The ideas and suggestions provided were such that we could get the best quality work done with least damage to our budget </p>
                                    <span className={`${styles.name}`}>Varun Harnal</span>
                                </div>
                            </div>
                        </div>
                    </Slider>
                </div>
            </div>
        </section>
    )
}

export default TestimonialSection
