import styles from '../../styles/BannerBar.module.css'
import Image from 'next/image'
import certificate1 from '../../public/certificate-1.png'
import certificate2 from '../../public/certificate-2.png'
import certificate3 from '../../public/certificate-3.png'
import certificate4 from '../../public/certificate-4.png'
import certificate5 from '../../public/certificate-5.png'

function BannerBar() {
    return (
        <section className={`${styles.bannerbar} container-fluid`}>
            <div className="container">
                <div className="row">
                    <div className="col-md-3 col-lg-4 d-flex justify-content-center align-items-center">
                        <p className="text-white text-center">Something Goes Here...</p>
                    </div>
                    <div className="col-md-9 col-lg-8 d-flex justify-content-around align-items-center">
                        <Image src={certificate3} alt="" className={styles.image} height={60} width={120} />
                        <Image src={certificate5} alt="" className={styles.image} height={80} width={100} />
                        <Image src={certificate1} alt="" className={styles.image} height={80} width={80} />
                        <Image src={certificate2} alt="" className={styles.image} height={80} width={80} />
                        <Image src={certificate4} alt="" className={styles.image} height={60} width={120} />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default BannerBar
