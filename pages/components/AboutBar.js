import styles from '../../styles/AboutBar.module.css'
import { useEffect, useState } from "react";

function AboutBar(props) {
    const [websitCounter, setWebsiteCounter] = useState(0);
    const [yearsCounter, setYearsCounter] = useState(0);
    const [marketingCounter, setMarketingCounter] = useState(0);

    if (props.offset > 500) {
        if (websitCounter === 0) {
            setWebsiteCounter(800);
            setYearsCounter(1);
            setMarketingCounter(30);
        }
    }

    useEffect(() => {
        websitCounter >= 800 && websitCounter < 1000 && setTimeout(() => setWebsiteCounter(websitCounter + 1), 0.3);
        yearsCounter >= 1 && yearsCounter < 8 && setTimeout(() => setYearsCounter(yearsCounter + 1), 80);
        marketingCounter >= 30 && marketingCounter < 50 && setTimeout(() => setMarketingCounter(marketingCounter + 1), 25);
    }, [websitCounter, yearsCounter, marketingCounter]);

    return (
        <section className={`${styles.aboutbar} container-fluid`}>
            <div className="container py-5  py-md-3">
                <div className="row justify-content-center">
                    <div className="col-10 col-md-4 col-lg-3 d-flex align-items-center justify-content-center flex-column">
                        <h3 className={`${styles.heading} text-center`}>{websitCounter}+</h3>
                        <p className={`${styles.para} text-center mt-2`}>Website design and development</p>
                    </div>
                    <div className="col-10 col-md-4 col-lg-3 d-flex align-items-center justify-content-center flex-column">
                        <h3 className={`${styles.heading} text-center`}>{yearsCounter}+</h3>
                        <p className={`${styles.para} text-center mt-2`}>years of exprience</p>
                    </div>
                    <div className="col-10 col-md-4 col-lg-3 d-flex align-items-center justify-content-center flex-column">
                        <h3 className={`${styles.heading} text-center`}>{marketingCounter}+</h3>
                        <p className={`${styles.para} text-center mt-2`}>
                            Developers, Designers and Digital Marketing ninjas
                        </p>
                    </div>
                </div>
            </div>

        </section>
    )
}

export default AboutBar
