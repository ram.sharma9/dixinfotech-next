import styles from '../../styles/Domain.module.css'
import Image from 'next/image'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import slide1 from "../../public/slide1.jpg"
import slide2 from "../../public/slide2.jpg"
import slide3 from "../../public/slide3.jpg"
import slide4 from "../../public/slide4.jpg"
import Link from 'next/link'

function DomainSection() {

    const settings = {
        dots: false,
        infinite: true,
        autoplay: true,
        speed: 500,
        responsive: [
            {
                breakpoint: 1900,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }
            }
        ]
    };

    return (
        <section className={`${styles.backgroundColor} container-fluid py-5`}>
            <div className="container py-3">
                <div className="row">
                    <div className="col-12">
                        <h2 className={`${styles.heading} text-center mt-0 mb-5`}>Domain experties</h2>
                    </div>
                    <div className="col-12">
                        <Slider {...settings}>
                            <div className="px-2 text-center">
                                <Link href="#">
                                    <a>
                                        <Image src={slide1} className={styles.image} fill="responsive" />
                                        <h3 className={`${styles.subHeading} text-center`}>Hospitality &amp; Travel</h3>
                                    </a>
                                </Link>
                            </div>
                            <div className="px-2 text-center">
                                <Link href="#">
                                    <a>
                                        <Image src={slide2} className={styles.image} fill="responsive" />
                                        <h3 className={`${styles.subHeading} text-center`}>E-commerce/ Retail</h3>
                                    </a>
                                </Link>
                            </div>
                            <div className="px-2 text-center">
                                <Link href="#">
                                    <a>
                                        <Image src={slide3} className={styles.image} fill="responsive" />
                                        <h3 className={`${styles.subHeading} text-center`}>E-Learning &amp; Education</h3>
                                    </a>
                                </Link>
                            </div>
                            <div className="px-2 text-center">
                                <Link href="#">
                                    <a>
                                        <Image src={slide4} className={styles.image} fill="responsive" />
                                        <h3 className={`${styles.subHeading} text-center`}>Healthcare</h3>
                                    </a>
                                </Link>
                            </div>
                            <div className="px-2 text-center">
                                <Link href="#">
                                    <a>
                                        <Image src={slide3} className={styles.image} fill="responsive" />
                                        <h3 className={`${styles.subHeading} text-center`}>One Other</h3>
                                    </a>
                                </Link>
                            </div>
                        </Slider>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default DomainSection
