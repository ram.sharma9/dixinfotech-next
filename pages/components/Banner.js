import styles from '../../styles/Banner.module.css'
import { Button } from '@material-ui/core'
import BannerBar from './BannerBar'

function Banner() {
    return (
        <section className={`${styles.banner} container-fluid`}>
            <div className="container h-100">
                <div className="row h-100">
                    <div className="col-12 d-flex justify-content-center align-items-center h-100 flex-column" >
                        <h1 className={styles.heading}>
                            Website Design, Development digital marketing
                        </h1>
                        <Button variant="contained" color="primary" size="large" className={`${styles.btn} mt-5`}>
                            Let’s Discuss next Project
                        </Button>
                    </div>
                </div>
            </div>
            <BannerBar />
        </section>
    )
}

export default Banner
