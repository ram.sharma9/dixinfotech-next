import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faMobile } from "@fortawesome/free-solid-svg-icons";
import { faSkype } from "@fortawesome/free-brands-svg-icons";
import Link from "next/link";
import styles from '../../styles/FooterForm.module.css'
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles(() => ({
    root: {
        '& .MuiTextField-root': {
            margin: '8px 0',
            width: '100%',
        },
    },
}));

function FooterForm() {
    const classes = useStyles();
    return (
        <section className={`${styles.footerform} container-fluid`}>
            <div className={`${styles.footerFormInner} container p-4 p-lg-5`}>
                <div className="row">
                    <div className="col-lg-7">
                        <div className="row">
                            <div className="col-12">
                                <h6 className={`${styles.subHeading} text-center mt-0 mb-0`}>Like what you see?</h6>
                                <h4 className={`${styles.heading} text-center mt-2 mb-0`}>Let&apos;s work together.</h4>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-md-4 text-center">
                                <div className={`${styles.svg_div}`}>
                                    <FontAwesomeIcon icon={faMobile} color="#000ba4" className={styles.svg} />
                                </div>
                                <p className={`${styles.para} mt-0 mb-0`}>Call/Whatsapp:</p>
                                <Link href="tel:+919717106162">
                                    <a>
                                        <p className={`${styles.subPara} mt-2 mb-0`} >+91-9717106162</p>
                                    </a>
                                </Link>
                                <Link href="tel:+919958973512">
                                    <a>
                                        <p className={`${styles.subPara} mt-1 mb-0`} >+91-9958973512</p>
                                    </a>
                                </Link>
                            </div>
                            <div className="col-md-4 text-center">
                                <div className={`${styles.svg_div}`}>
                                    <FontAwesomeIcon icon={faEnvelope} color="#000ba4" className={styles.svg} />
                                </div>
                                <p className={`${styles.para} mt-0 mb-0`}>Email Us</p>
                                <Link href="mailto:info@dixinfotech.com">
                                    <a>
                                        <p className={`${styles.subPara} mt-2 mb-0`}>info@dixinfotech.com</p>
                                    </a>
                                </Link>
                            </div>
                            <div className="col-md-4 text-center">
                                <div className={`${styles.svg_div}`}>
                                    <FontAwesomeIcon icon={faSkype} color="#000ba4" className={styles.svg} />
                                </div>
                                <p className={`${styles.para} mt-0 mb-0`}>Skype:</p>
                                <Link href="#">
                                    <a>
                                        <p className={`${styles.subPara} mt-2 mb-0`}>Dixinfotech</p>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5 mt-5 mt-lg-0">
                        <div className="row">
                            <div className="col-12">
                                <h6 className={`${styles.subHeading} text-center mt-0 mb-0`}>Looking something Intresting?</h6>
                                <h4 className={`${styles.heading} text-center mt-2 mb-0`}>Get a Free Quote.</h4>
                            </div>
                        </div>
                        <div className="row mt-4 p-2 p-lg-0">
                            {/* <form className="col-12"> */}
                            <form className={classes.root} noValidate autoComplete="off">
                                <div>
                                    <TextField label="Name" required variant="outlined" />
                                    <TextField label="Email" required variant="outlined" />
                                    <TextField label="Phone" required variant="outlined" />
                                    <TextField label="What is your project about?" maxRows={4} multiline required variant="outlined" />
                                    <Button size="large" variant="contained" className={`${styles.btn} mt-2`}> Send Your Inquery </Button>
                                </div>
                            </form>
                            {/* </form> */}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default FooterForm
