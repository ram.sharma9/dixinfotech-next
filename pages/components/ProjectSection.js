import styles from '../../styles/ProjectSection.module.css'
import Image from 'next/dist/client/image'
import projectBanner from '../../public/project-banner.png'
import { Button } from '@material-ui/core'

function ProjectSection() {
    return (
        <section className="container py-5">
            <div className="row px-3 px-lg-0">
                <div className="col-12">
                    <h4 className={`${styles.heading} text-center mb-4`}>Latest Projects</h4>
                </div>
                <div className="col-lg-7 p-0">
                    <Image src={projectBanner} alt="" className={styles.image} layout="responsive" />
                </div>
                <div className={`${styles.text_box} col-lg-5 p-4 p-lg-3 p-xl-4`}>
                    <h4 className={`${styles.subHeading} text-white mt-0 mt-xl-2 mb-2`}>Planet Vedic</h4>
                    <p className={`${styles.para} text-white mt-0 mb-0`}>Planetvedic.com is a online astrlogy marketplace. This
                        portal provides various tyes of servers related to the
                        Vedic astrology, Palm reading, numorology, Vastu etc.</p>
                    <h5 className={`${styles.smallHeading} text-white mb-2 mt-4 mt-lg-3 mt-xl-4`}>Technology Used</h5>
                    <p className={`${styles.para} text-white mt-0 mb-0`}>PHP, Laravel, MySql HTML, CSS, Reactjs</p>
                    <Button variant="outlined" color="primary" size="large" className={`${styles.btn} mt-4 mt-lg-3 mt-xl-4`}>
                        KNOW MORE
                    </Button>
                </div>
                <div className="col-12 text-center">
                    <Button variant="contained" color="primary" size="large" className={`${styles.loadBtn} mt-4 mt-lg-3 mt-xl-4`}>
                        VIEW ALL
                    </Button>
                </div>
            </div>
        </section>
    )
}

export default ProjectSection
