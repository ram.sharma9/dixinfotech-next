import styles from '../../styles/Footer.module.css'
import DeviceIdentifier from 'react-device-identifier';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

function FooterAccordion(props) {
    let paras = props.para.map(function (value, i) {
        return (
            <>
                <a href="#" key={i}>
                    <p className={`${styles.mobPara}`} key={i}>
                        {value}
                    </p>
                </a>
            </>
        );
    });
    return (
        <Accordion>
            <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                <h3 className={`${styles.mobHeading} my-0`}>{props.heading}</h3>
            </AccordionSummary>
            <AccordionDetails>
                <div> {paras} </div>
            </AccordionDetails>
        </Accordion>
    )
}

function Footer() {
    return (
        <>
            <footer className={`${styles.footer} container-fluid pt-5`}>
                <div className="container pb-5">

                    <DeviceIdentifier isMobile={true} isTablet={false} isDesktop={false}>
                        <Accordion>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                                <h3 className={`${styles.mobHeading} my-0`}>Web/ Graphic Design</h3>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div>
                                    <a href="#"><p className={`${styles.mobPara}`}>CMS Theme Design</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Corporate Identity Design</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Ecommerce Website Design</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Landing Page Design</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Logo Design</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Mobile Apps UI Design</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Mobile Site Design</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Responsive Website Design</p></a>
                                </div>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                                <h3 className={`${styles.mobHeading} my-0`}>Web/ App Development</h3>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div>
                                    <a href="#"><p className={`${styles.mobPara}`}>Ecommerce Website Development</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Magento Development</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>WordPress Development</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Shopify Development</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Custom Web Development Company</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Angular JS</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Laravel Development</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Joomla Development</p></a>
                                </div>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                                <h3 className={`${styles.mobHeading} my-0`}>Digital Marketing</h3>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div>
                                    <a href="#"><p className={`${styles.mobPara}`}>Ecommerce Marketing</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Youtube Video Marketing</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>SMS Marketing</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Email Marketing</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Search Engine Marketing – PPC</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Social Media Optimisation</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Best Search Engine Optimization Company</p></a>
                                </div>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                                <h3 className={`${styles.mobHeading} my-0`}>Domain/ Server/Email</h3>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div>
                                    <a href="#"><p className={`${styles.mobPara}`}>Business Email Solutions</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Domain Name Registration</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Google Apps Service</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Hosting</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Website Maintenance Services</p></a>
                                </div>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                                <h3 className={`${styles.mobHeading} my-0`}>Website Development By Industry</h3>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div>
                                    <a href="#"><p className={`${styles.mobPara}`}>Construction</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>E-Learning &amp; Education</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Energy &amp; Utilities</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Environmental Management</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Finance</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Food &amp; Beverage</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Hospitality &amp; Travel</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Legal</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Media &amp; Entertainment</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Nonprofit</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Oil & Gas</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Retail</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Real Estate</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}></p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Supply Chain</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Telecommunications</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Transportation</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>manufacturing/ engineering</p></a>
                                </div>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                                <h3 className={`${styles.mobHeading} my-0`}>Website Development By Services</h3>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div>
                                    <a href="#"><p className={`${styles.mobPara}`}>Travel Website Development</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Hotel Website Development Company</p></a>
                                    <a href="#"><p className={`${styles.mobPara}`}>Flight Booking Website Development</p></a>
                                </div>
                            </AccordionDetails>
                        </Accordion>
                    </DeviceIdentifier>

                    <DeviceIdentifier isMobile={false} isTablet={true} isDesktop={true}>
                        <div className="row">
                            <div className="col-12 col-sm-6 col-lg-3 mt-4">
                                <h6 className={`${styles.heading} mt-0 mb-4`} > Web/ Graphic Design </h6>
                                <a href="#"><p className={`${styles.mobPara}`}> CMS Theme Design </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Corporate Identity Design </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Ecommerce Website Design </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Landing Page Design </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Logo Design </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Mobile Apps UI Design </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Mobile Site Design </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Responsive Website Design </p></a>
                            </div>
                            <div className="col-12 col-sm-6 col-lg-3 mt-4">
                                <h6 className={`${styles.heading} mt-0 mb-4`} > Web/ App Development </h6>
                                <a href="#"><p className={`${styles.mobPara}`}> Ecommerce Website Development </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Magento Development </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> WordPress Development </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Shopify Development </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Custom Web Development Company </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Angular JS </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Laravel Development </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Joomla Development </p></a>
                            </div>
                            <div className="col-12 col-sm-6 col-lg-3 mt-4">
                                <h6 className={`${styles.heading} mt-0 mb-4`} > Digital Marketing </h6>
                                <a href="#"><p className={`${styles.mobPara}`}> Ecommerce Marketing </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Google Local Business SEO Services </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Youtube Video Marketing </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> SMS Marketing </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Email Marketing </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Search Engine Marketing – PPC </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Social Media Optimisation </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Best Search Engine Optimization Company </p></a>
                            </div>
                            <div className="col-12 col-sm-6 col-lg-3 mt-4">
                                <h6 className={`${styles.heading} mt-0 mb-4`} > Domain/ Server/Email </h6>
                                <a href="#"><p className={`${styles.mobPara}`}> Business Email Solutions </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Domain Name Registration </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Google Apps Service </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Hosting </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Website Maintenance Services </p></a>
                            </div>
                            <div className="col-12 col-sm-6 col-lg-3 mt-4">
                                <h6 className={`${styles.heading} mt-0 mb-4`} > Website Development By Industry </h6>
                                <a href="#"><p className={`${styles.mobPara}`}> Construction </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> E-Learning &amp; Education </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Energy &amp; Utilities </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Environmental Management </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Finance </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Food &amp; Beverage </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Hospitality &amp; Travel </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Legal </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Media &amp; Entertainment </p></a>
                            </div>
                            <div className="col-12 col-sm-6 col-lg-3 mt-4">
                                <h6 className={`${styles.heading} mt-0 mb-4`} >  </h6>
                                <a href="#"><p className={`${styles.mobPara}`}> Media & Entertainment </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Nonprofit </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Oil & Gas </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Retail </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Real Estate </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Sports </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Supply Chain </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Telecommunications </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Transportation </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Manufacturing/ Engineering </p></a>
                            </div>
                            <div className="col-12 col-sm-6 col-lg-3 mt-4">
                                <h6 className={`${styles.heading} mt-0 mb-4`} > Website Development By Services </h6>
                                <a href="#"><p className={`${styles.mobPara}`}> Travel Website Development </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Hotel Website Development Company </p></a>
                                <a href="#"><p className={`${styles.mobPara}`}> Flight Booking Website Development </p></a>
                            </div>
                        </div>
                    </DeviceIdentifier>
                </div>
            </footer>
            <section className={`${styles.copyrightContainer} container-fluid py-3`}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-xl-4 d-flex justify-content-center align-items-center align-items-lg-start flex-column order-2 order-md-1 mt-4 mt-md-0">
                            <p className={`${styles.copyright} my-0 text-center`}>Copyright 2013-2021, Dixinfotech Services LLP</p>
                        </div>
                        <div className="col-lg-9 col-xl-8 order-1 order-md-2">
                            <ul className={`${styles.list} d-flex flex-column flex-md-row justify-content-around`} >
                                <a href="#"><li className={`${styles.listItem} mt-3`}> Career </li></a>
                                <a href="#"><li className={`${styles.listItem} mt-3`}> Training </li></a>
                                <a href="#"><li className={`${styles.listItem} mt-3`}> Contact Us </li></a>
                                <a href="#"><li className={`${styles.listItem} mt-3`}> Cancellations/ Refund Policy </li></a>
                                <a href="#"><li className={`${styles.listItem} mt-3`}> Privacy Policy </li></a>
                                <a href="#"><li className={`${styles.listItem} mt-3`}> Terms &amp; Conditions </li></a>
                                <a href="#"><li className={`${styles.listItem} mt-3`}> Blog </li></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Footer







