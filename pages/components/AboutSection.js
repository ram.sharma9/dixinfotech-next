import styles from '../../styles/AboutSection.module.css'
import Image from 'next/dist/client/image'
import about from '../../public/about.png'

function AboutSection() {
    return (
        <section className="container py-5">
            <div className="row">
                <div className="col-12 col-lg-6 order-2 order-lg-1 mt-4 mt-lg-0">
                    <h4 className={styles.heading}>
                        About Us
                    </h4>
                    <p className={styles.para}>
                        The most important aspect in the Business world is having an attractive and user-friendly Website. So, we at Dixinfotech – Web Design Company in India, with a contingent of experienced and creative superheroes, wizards, architects, and hunters aim to provide you with holistic, fresh, and young marketing ideas to help your business achieve great excellence beating down your competitors. We take an integrated approach unlike our rivals to create a highly engaging digital brand for your company focusing on high-end solutions.
                    </p>
                    <p className={styles.para}>
                        Being a set of warriors we have undertaken the Digital Marketing world to create a portfolio of custom Web Designing, Web Development, Domain Hosting, and SEO/SEM Services for companies in a variety of industries like eCommerce Website, hotel, manufacturing, and others with expertise that’s why we become best web design company in India. We leave no stone unturned and so we also specialize in Logo Designing, Brochure Designing, and Stationery Designing, etc.
                    </p>
                </div>
                <div className="col-12 col-lg-6 d-flex align-items-center justify-content-center order-1 order-lg-2">
                    <Image src={about} />
                </div>
            </div>

        </section>
    )
}

export default AboutSection
